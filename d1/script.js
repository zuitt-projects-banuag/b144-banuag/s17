console.log("Array Manipulation");

//Basic Array Structure
//Access Elements in an Array - through index

// Two ways to initialize an Array

let array = [1,2,3];
console.log(array);

 let arr = new Array(1, 2, 3);
 console.log(arr);

 // console.log(array[0]);
 // console.log(array[1]);
 // console.log(array[2]);

 /*
 Formulation
 index = array.length - 1
 */

 //Array Manipulation - manipulate or changing the data
 let count = ["one","two","three","four"];

 console.log(count.length);
 console.log(count[4]);

 // Using assignment operator (=)
 count[4] = "five";
 console.log(count)

 /*Reassignment
 count[0] = "element";
 console.log(count);*/

 //Push method array.push()
 	//add element at the end of an array
 count.push("element");
 console.log(count)

 function pushMethod(element){
 	return count.push(element);
 }
 pushMethod("six");
 pushMethod("seven");
 pushMethod("eight");

 console.log(count);

 //Pop Method Syntax: array.pop()
 	//Removes the last elemet of an array
 count.pop();
 console.log(count)

 function popMethod() {
 	count.pop()
 }
 popMethod()
 console.log(count)

 //Unshift Method
 count.unshift("hugot")
 console.log(count)

 function unshiftMethod(name) {
 	return count.unshift(name)
 }
 unshiftMethod("zero")
 console.log(count)

 //Shift Method Syntax: array.shift()
 	//removes an element at the beginning of an array
 count.shift()
 console.log(count)

 function shiftMethod() {
 	count.shift()
 }
 shiftMethod()
 console.log(count)

 //Sort Method Syntax: array.sort()
 let nums = [15, 32, 61, 130, 230, 13, 34]
 nums.sort();
 console.log(nums)

 //sort nums in ascending order
 nums.sort(
 		// a is a previous value, b is current value
 		function(a, b) {
 			return a - b;
 		}
 	);
 console.log(nums);

 // sort nums in descending order
 nums.sort(
 		// a is a previous value, b is current value
 		function(a, b) {
 			return a - b;
 		}
 	);
 console.log(nums);
 //Reverse Method Syntax: array.reverse()
 nums.reverse()
 console.log(nums)

 /*Splice and Slice

	Splice Method 
		Syntax: array.splice()

		return of array of omitted elements
		it directly manipulates the original array
	First Parameter - Index where to start omitting element
	Second Parameter - # of elements to be omitted starting from first parameter
	Third Paramater = elements to be added in place of the omitted elements

	Use splice if you want to remove elements in the middle of an array
 */
 //console.log(count)

 // let newSplice = count.splice(1);
 // console.log(newSplice)
 // console.log(count)

 // let newSplice = count.splice(1, 2);
 // console.log(newSplice);
 // console.log(count);

 // let newSplice = count.splice(1, 2, "a1", "b2", "c3")
 // console.log(newSplice)
 // console.log(count)

 /*Slice Method Syntax: array.slice(start, end)
 	nagrereturn din ng array pero copy of omitted yung nirereturn.

 	First Parameter - Index where to begin omitting elements
 	Second Parameter - # of elements to be omitted (index - 1)
 	starting index to end
 */
 // let newSlice = count.slice(1);
 // console.log(newSlice)
 // console.log(count)

 console.log(count)
 
 let newSlice = count.slice(2, 3);
 console.log(newSlice);

 //Concat Method Syntax: array.concat()
 	//use to merge two or more arrays
 console.log(count)
 console.log(nums)
 let animals = ["bird", "cat", "dog", "fish"];

 let newConcat = count.concat(nums, animals)
 console.log(newConcat);

 //Join Method Syntax: array.join() Paramters: "", '-', " "
 let meal = ["rice", "steak", "juice"];
 let newJoin = meal.join()
 console.log(newJoin)

 newJoin = meal.join("")
 console.log(newJoin)

 newJoin = meal.join(" ")
 console.log(newJoin)

 newJoin = meal.join("-")
 console.log(newJoin)

 //toString Method
 console.log(nums)
 console.log(typeof nums[3])

 let newString = nums.toString()
 console.log(typeof newString)

/*Accesssors*/
	let countiers = ["US", "PH", "CAN", "PH", "SG", "HK", "PH", "NZ"];
	//indexOf()
		//Finds the index of a given element where it is "first" found.
	let index = countiers.indexOf("PH")
		console.log(index)
	//If element is non existing, return is -1
	let example = countiers.indexOf("AU")
		console.log(example)

	//lastIndex()
	let lastIndex = countiers.lastIndexOf("PH");
	console.log(lastIndex);

	/*if(countiers.indexOf("CAN") == -1){
		console.log(`Element not existing`)
	}else{
		console.log(`Element exits in the countiers array`)
	}*/

	/*Iterators */
	//forEach(callbackfunction/cb()) Syntax: array.forEach()

	//map() Syntax: array.map()

	let days = ["mon","tue","wed","thu","fri","sat","sun"];
	//forEach
		//Return undefined
	// let dayOftheweek = days.forEach(
	// 		function(element){
	// 			console.log(element);
	// 		}
	// 	)
	
	//map
		//return a copy of an array from the original array which can be manipulated
	let mapDays = days.map(function(day){
		return `${day} is the day of the week`
	})
	console.log(mapDays)
	console.log(days)

	//Mini Activity

	days2 = days.forEach(
			function(days2){
			console.log(days2)	
			}
		) 

	//filter Syntax: array.filter(cb())
		console.log(nums)

		let newFilter = nums.filter(function(num){
			return num < 50
		})

		console.log(newFilter)

		//includes Syntax: array.includes()
			//returns boolean value if a word is existing in an array
		console.log(animals);

		let newIncludes = animals.includes("dog")
		console.log(newIncludes)

		// Mini Activity
		let values
		function valueActivity(value) {
			if(animals.includes(name)){
				return `${name} is found`
			} else {
				return `${name} not found`
			}
		}

		console.log(valueActivity("dog")); 
		console.log(valueActivity("cat")); 
		console.log(valueActivity("fish"));

		//every(cb())
			//returns boolean
			//returns true only if "all elements" passed the given condition
			let newEvery = nums.every(function(num){
				return(num > 10)
			})

			console.log(newEvery);

		//some(cb())
			//boolean
			let newSome = nums.every(function(num){
				return(num > 50)
			})

			console.log(newSome);

		//Arrow Function
		// 

		// reduce(cb())
		let newReduce = nums.reduce(function(a, b){
			  return a + b
			//return b - a
		})
		console.log(newReduce)

		//To get the average of nums array
			//Total all the elements
			//Divide it by total # of elements
		// console.log(newReduce/nums.length)
		let average = newReduce/nums.length
// -------------------------------------------------
		//toFixed(# of decimals) - returns string
		console.log(average.toFixed(2))

		//parseInt() or parseFloat()
		console.log(parseInt(average.toFixed(2)));
		console.log(parseFloat(average.toFixed(2)));

